package radar.sprot.sportradardgtest

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View

class VolumeView(context: Context, attrs: AttributeSet) : View(context, attrs) {
    private var rectList = mutableListOf<Rect>()
    private val paint = Paint()
    private var lines: Int = 10
    private var volume: Int = 0
    private var viewWidth: Int = 0
    private var originalLines: Int = 10
    private var listener: OnVolumeChangedListener? = null
    private var volumeColor: Int = Color.BLUE

    private var typedArray = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.VolumeView,
            0, 0)

    init {
        try {
            lines = typedArray.getInteger(R.styleable.VolumeView_lines, 10)
            originalLines = typedArray.getInteger(R.styleable.VolumeView_lines, 10)
            volume = typedArray.getInteger(R.styleable.VolumeView_currentVolumePercent, 0)
            volumeColor = typedArray.getColor(R.styleable.VolumeView_volumeColor, Color.BLUE)
        } finally {
            typedArray.recycle()
        }
    }

    private fun createRect() {
        rectList.clear()
        var off = 0
        val size = lines / originalLines
        if (size > 0) {
            for (i in 1..lines) {
                off += 55 / size
                val element = Rect(0, 0, viewWidth, 50 / size)
                element.offset(0, off)
                rectList.add(element)
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        viewWidth = measuredWidth
        createRect()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        paint.color = Color.GRAY
        val perc = rectList.size / 100F * volume
        for (i in 0 until rectList.size) {
            val rect = rectList[i]
            if (i + 1 > rectList.size - Math.round(perc)) {
                paint.color = volumeColor
            }
            canvas.drawRect(rect, paint)
        }
        listener?.onVolumeChange(volume)
    }

    fun increaseVolume() {
        if (volume <= 99) {
            volume += 1
            invalidate()
        }
    }

    fun decreaseVolume() {
        if (volume >= 1) {
            volume -= 1
            invalidate()
        }
    }

    fun setVolume(volume: Int) {
        if (volume / 10 <= lines) {
            this.volume = volume
            invalidate()
        }
    }

    fun drawVolumeWithLines(lines: Int) {
        this.lines = lines
        createRect()
        invalidate()
    }

    fun setListener(listener: OnVolumeChangedListener) {
        this.listener = listener
    }

    interface OnVolumeChangedListener {
        fun onVolumeChange(volume: Int)
    }

    companion object {
        const val RECT_RIGHT = 800
    }
}