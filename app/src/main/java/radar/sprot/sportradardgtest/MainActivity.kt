package radar.sprot.sportradardgtest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var initialY: Float = 0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        volumeBtn.setOnClickListener {
            val vol = volumeEt.text.toString()
            if (vol.isNotEmpty()) {
                volumeView.setVolume(vol.toInt())
            }
        }
        linesBtn.setOnClickListener {
            val lines = linesEt.text.toString()
            if (lines.isNotEmpty()) {
                volumeView.drawVolumeWithLines(lines.toInt())
            }
        }
        volumeView.setListener(object : VolumeView.OnVolumeChangedListener {
            override fun onVolumeChange(volume: Int) {
                val volumeVal = volume.toString() + " %"
                currentVolume.text = volumeVal
            }
        })
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return when (event.action) {
            MotionEvent.ACTION_MOVE -> {
                if (initialY < event.y) {
                    volumeView.decreaseVolume()
                } else {
                    volumeView.increaseVolume()
                }
                initialY = event.y
                true
            }
            else -> {
                super.onTouchEvent(event)
            }
        }
    }
}
